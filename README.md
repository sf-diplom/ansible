# Настройка хстов была выполнена с помощью системы управления конфигурацией Ansible


## В работе использовались Ansible Playbook.

#### Проект состоит из файлов:
- Инвентори файл "hosts"
- Плэйбук "control-plane.yaml"
- Плэйбук "dependencies.yaml" 
- Плэйбук "cluster-init.yaml"
- Директория "scripts"


## Описание файлов

- Файл "hosts" содержит ip адреса хостов для управления, а также имена пользователей на этих хостах.
- Плэйбук "dependencies.yml" содержит необходимый софт для проекта. После применения данного плейбука, серверы будут сконфигурированны следующем образом:
    - На серверы srv, master и app будут установлены docker engine, apt transport https, kublet, kubeadm, gitlab runner.
- С помощью плейбука "control-plane.yml" на сервер master будут установлеы kubectl, helm.
- Плейбук "claster-init.yml" производит инициализацию кластера Kubernetes.
- Директория "scripts" содержит bash-скрипты, необходимые для установки Docker и Ansible.

## Для применения ansible-playbook необходимо:

- Клонировать текущий репозиторий командой git clone https://gitlab.com/sf-diplom/ansible.git.
- Перейти в локальную папку с репозиторием.
- Запустить плейбуки в следующей последовательности.
    - ansible-playbook -i hosts dependencies.yaml
    - ansible-playbook -i hosts control-plane.yaml
    - ansible-playbook -i hosts claster-init.yaml